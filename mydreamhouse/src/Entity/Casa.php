<?php

namespace App\Entity;

use App\Repository\CasaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CasaRepository::class)
 */
class Casa
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $direccion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $tipo_venta;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $m2;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $precio;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $tipo_casa;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $propietario;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $email_casa;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fecha_constr;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $imagen_casa;

    /**
     * @ORM\ManyToOne(targetEntity=Ciudad::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_ciu;

    /**
     * @ORM\ManyToOne(targetEntity=Usuario::class)
     */
    private $id_usu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    public function setDireccion(string $direccion): self
    {
        $this->direccion = $direccion;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(?int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getTipoVenta(): ?string
    {
        return $this->tipo_venta;
    }

    public function setTipoVenta(?string $tipo_venta): self
    {
        $this->tipo_venta = $tipo_venta;

        return $this;
    }

    public function getM2(): ?int
    {
        return $this->m2;
    }

    public function setM2(?int $m2): self
    {
        $this->m2 = $m2;

        return $this;
    }

    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    public function setPrecio(?int $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getTipoCasa(): ?string
    {
        return $this->tipo_casa;
    }

    public function setTipoCasa(?string $tipo_casa): self
    {
        $this->tipo_casa = $tipo_casa;

        return $this;
    }

    public function getPropietario(): ?string
    {
        return $this->propietario;
    }

    public function setPropietario(?string $propietario): self
    {
        $this->propietario = $propietario;

        return $this;
    }

    public function getTelefono(): ?int
    {
        return $this->telefono;
    }

    public function setTelefono(?int $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmailCasa(): ?string
    {
        return $this->email_casa;
    }

    public function setEmailCasa(?string $email_casa): self
    {
        $this->email_casa = $email_casa;

        return $this;
    }

    public function getFechaConstr(): ?int
    {
        return $this->fecha_constr;
    }

    public function setFechaConstr(?int $fecha_constr): self
    {
        $this->fecha_constr = $fecha_constr;

        return $this;
    }

    public function getImagenCasa(): ?string
    {
        return $this->imagen_casa;
    }

    public function setImagenCasa(?string $imagen_casa): self
    {
        $this->imagen_casa = $imagen_casa;

        return $this;
    }

    public function getIdCiu(): ?Ciudad
    {
        return $this->id_ciu;
    }

    public function setIdCiu(?Ciudad $id_ciu): self
    {
        $this->id_ciu = $id_ciu;

        return $this;
    }

    public function getIdUsu(): ?Usuario
    {
        return $this->id_usu;
    }

    public function setIdUsu(?Usuario $id_usu): self
    {
        $this->id_usu = $id_usu;

        return $this;
    }
}
