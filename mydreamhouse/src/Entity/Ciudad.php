<?php

namespace App\Entity;

use App\Repository\CiudadRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CiudadRepository::class)
 */
class Ciudad
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $nombre_ciudad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreCiudad(): ?string
    {
        return $this->nombre_ciudad;
    }

    public function setNombreCiudad(string $nombre_ciudad): self
    {
        $this->nombre_ciudad = $nombre_ciudad;

        return $this;
    }

    public function __toString()
    {
        return $this->nombre_ciudad;
    }
}
