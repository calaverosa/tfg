<?php

namespace App\Form;

use App\Entity\Casa;
//use Doctrine\DBAL\Types\DateType;
//use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Validator\Constraints\File;

class CasaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('direccion', TextType::class)
            ->add('cp', NumberType::class)
            ->add('tipo_venta', ChoiceType::class, [
                'choices' => [
                    'Se vende' => 'Se vende',
                    'Se alquila' => 'Se alquila',
                ],
                'expanded' => true,
                'multiple' => false,
                'label' => 'Tipo de venta',
                'required' => true
            ])
            ->add('m2', NumberType::class)
            ->add('precio', NumberType::class)
            ->add('tipo_casa', ChoiceType::class, [
                'choices' => [
                    'Apartamento' => 'Apartamento',
                    'Ático' => 'Ático',
                    'Bajo' => 'Bajo',
                    'Duplex' => 'Duplex',
                    'Loft' => 'Loft',
                    'Chalet' => 'Chalet',
                    'Finca' => 'Finca'
                ],
                'expanded' => false,
                'multiple' => false,
                'label' => 'Tipo de casa',
                'required' => true
            ])
            ->add('propietario', NumberType::class)
            ->add('telefono', NumberType::class)
            ->add('email_casa', FileType::class, [
                'label' => 'Foto',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/gif',
                        ],
                        'mimeTypesMessage' => 'Por favor introduce un formato de imagen válido',
                    ])
                ],
            ])
            ->add('fecha_constr', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('imagen_casa', FileType::class, [
                'label' => 'Foto',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '2048k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                            'image/gif',
                        ],
                        'mimeTypesMessage' => 'Por favor introduce un formato de imagen válido',
                    ])
                ],
            ])
            ->add('id_ciu', EntityType::class, [
                'required' => true,
                'label' => 'Ciudad',
                'placeholder' => 'Selecciona ciudad...',
                'class' => 'App:Ciudad'
            ])
            /* ->add('id_usu') */;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Casa::class,
        ]);
    }
}
